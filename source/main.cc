#include <cmath>
#include <cstdlib>

#include <exception>
#include <iostream>
#include <string>

#include <fun.hh>

/// For a valid C / C++ program, you need one main function.
int main(int argc, char *argv[]) {
	// write a welcome message to the console
	std::cout << "Welcome to app!" << std::endl << std::endl;
	
	// output the actual number of arguments given to the program
	std::cout << "Argument count (argc) = " << argc << std::endl;
	
	// output the actual argument values given to the program
	std::cout << "Argument values (argv[]):" << std::endl;
	for (int i(0); i < argc; ++i) {
		std::cout << (i+1) << ")\t" << argv[i] << std::endl;
	}
	
	// use function fun to double the given argument value #2
	if (argc != 2) {
		std::cerr << "usage ./app <input number>" << std::endl;
		return 1;
	}
	
	const std::string str(argv[1]);
	const double inp = std::stod(str);
	const double res = fun(inp);
	
	std::cout << "input = \t" << inp << std::endl;
	std::cout << "result = \t" << res << std::endl;
	
	// exit the program gently
	return 0;
}
